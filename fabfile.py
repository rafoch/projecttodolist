from __future__ import with_statement
from fabric.api import env, task, run, local, prefix, settings, sudo, cd
from contextlib import contextmanager

env.hosts = ['151.80.60.57']
env.user = 'todolist'
env.key_filename = '/Users/lukas/.ssh/id_rsa'
env.port = '56'
env.activate = 'source env/bin/activate'

@task
def deploy():
    with cd('projecttodolist/'):
        run('git checkout .')
        run('git pull')
    with cd('projecttodolist/src/main/java/com/todolist/config/'):
        run('rm HibernateConfig.java')
        run('cp HibernateConfigProd.java.example HibernateConfig.java')
    with cd('projecttodolist/'):
        run('mvn package')
    with settings(user='lukas'):
        sudo('rm -rf  /var/lib/tomcat8/webapps/*')
        sudo('cp /var/webapps/todolist/projecttodolist/target/ProjectToDoList-1.0-SNAPSHOT.war /var/lib/tomcat8/webapps/ROOT.war')
    