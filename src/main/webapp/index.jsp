<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@include file="header.jsp" %>

<div class="wrapper">
    <%@include file="sidebar.jsp" %>

    <!-- Page Content Holder -->
    <div id="content">

        <%@include file="navbar.jsp" %>

        <h2 class="text-center">Po prostu działaj skutecznie!</h2>
        <p class="text-center">Zapracowani ludzie efektywnie zarządzają projektami dzięki aplikacji
            <strong>ToDoList</strong>!</p>

        <div class="line"></div>

        <h2>Produktywność</h2>
        <p>
            Pomagamy setkom tysięcy zapracowanych profesjonalistów i ich zespołom oraz dużym i małym firmom w skutecznym załatwianiu spraw. Dzięki naszym intuicyjnej aplikacji szybko staniesz się ninja produktywności. Dowiesz się, jak radzić sobie z zadaniami, zarządzać nimi wewnątrz projektów oraz priorytetyzować i realizować cele szybko i skutecznie.
        </p>
        <div class="line"></div>
        <h2>Mobilność</h2>
        <p>Szybko zorganizujesz się dzięki prostemu interfejsowi webowemu, który dopasowuje się do każdej rozdzielczości ekranu! A to nie wszystko! Załatwiaj sprawy gdziekolwiek, kiedykolwiek i jakkolwiek chcesz dzięki darmowym aplikacjom Nozbe na komputery, smartfony i tablety.</p>
        <div class="line"></div>

        <h2>Bezpieczeństwo</h2>
        <p>Jesteśmy dumni z naszej infrastruktury serwerowej, która została zaprojektowana z myślą o bezpieczeństwie danych użytkowników.</p>
    </div>
</div>

<%@include file="footer.jsp" %>