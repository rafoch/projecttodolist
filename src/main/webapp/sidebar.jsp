<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!-- Sidebar Holder -->
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>ToDoList</h3>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="/"><span class="glyphicon glyphicon-home"></span> Strona Główna</a>
        </li>
        <c:choose>
            <c:when test="${sessionScope.User != null}">
                <li>
                    <spring:url value="/users/${sessionScope.User.id}" var="userDetail" />
                    <a href="${userDetail}"><span class="glyphicon glyphicon-user"></span> Profil</a>
                </li>
                <li>
                    <spring:url value="/logout" var="logout" />
                    <a href="${logout}"><span class="glyphicon glyphicon-log-out"></span> Wyloguj się</a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="/login"><span class="glyphicon glyphicon-log-in"></span> Logowanie</a>
                </li>
                <li>
                    <a href="/register"><span class="glyphicon glyphicon-plus"></span> Rejestracja</a>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>
    <c:if test="${sessionScope.IsAdmin}">
        <ul class="list-unstyled components">
            <li>
                <a href="/admin/users"><span class="glyphicon glyphicon-th-list"></span> Lista Użytkowników</a>
            </li>
        </ul>
    </c:if>
    <c:choose>
        <c:when test="${sessionScope.User != null}">
            <ul class="list-unstyled components">
                <li style="margin-bottom: 15px">
                    <a href="/projects"><span class="glyphicon glyphicon-th-list"></span> Lista Projektów</a>
                </li>
            <c:forEach items="${sessionScope.projectList}" var="project">
                    <li>
                        <spring:url value="/projects/${project.id}" var = "projectURL" />
                        <a data-id="${project.id}" href="${projectURL}"><span class="glyphicon glyphicon-fire"></span>
                            <span class="name">${project.name}</span>
                        </a>
                    </li>
            </c:forEach>
        </ul>
        </c:when>
    </c:choose>

</nav>