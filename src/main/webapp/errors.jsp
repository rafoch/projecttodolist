<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:forEach items="${sessionScope.errors}" var="error">
    <div class="alert alert-danger alert-dismissable fade in sudo-center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            ${error}
    </div>
</c:forEach>
<% session.setAttribute("errors", new ArrayList<String>()); %>