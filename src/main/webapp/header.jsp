<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>ToDoList</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="<c:url value="/resources/static/css/bootstrap.min.css" />">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="<c:url value="/resources/static/css/style.css" />">
</head>
<body>

<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>