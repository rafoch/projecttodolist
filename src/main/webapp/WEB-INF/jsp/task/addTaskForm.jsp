<%--
  Created by IntelliJ IDEA.
  User: Carlton Leatch
  Date: 20.10.2017
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<spring:url value="/project/saveTask" var="addProjectURL" />
<form:form action="${addProjectURL}" method="POST" modelAttribute="addTaskForm">
   ${pageContext.request.contextPath}
    <form:hidden path="id"/>
    <table>
        <tr>
            <td>Task:</td>
            <td><form:input path="task"/></td>
        </tr>
        <tr>
            <td></td>
            <td><button type="submit">Save</button> </td>
        </tr>

    </table>
</form:form>
</body>
</html>
