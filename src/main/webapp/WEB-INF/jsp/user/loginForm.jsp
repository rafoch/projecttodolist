<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@include file="../../../header.jsp" %>

<div class="wrapper">
    <%@include file="../../../sidebar.jsp" %>

    <!-- Page Content Holder -->
    <div id="content">

        <%@include file="../../../navbar.jsp" %>

        <h2 class="text-center header">Zaloguj się, ale już!</h2>
        <%@include file="../../../errors.jsp" %>
        <spring:url value="/login" var="loginURL"/>
        <form:form action="${loginURL}" method="POST" modelAttribute="loginForm" cssClass="form-center">
            <div class="form-group">
                <label>Nazwa użytkownika:</label>
                <form:input path="username" pattern="\w{4,}" required="required" title="Proszę podać minimum 4 znaki"
                            cssClass="form-control"/>
            </div>
            <div class="form-group">
                <label>Hasło:</label>
                <form:input type="password" path="password"
                            pattern="\w{8,}" required="required" title="Proszę podać minimum 8 znaków"
                            cssClass="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary">Zaloguj</button>
            <a class="btn btn-danger btn-number pull-right" data-toggle="modal" data-target="#forgotPasswordModal">
                <span>Zapomniałeś hasła?</span>
            </a>
            <div class="clearfix"></div>
        </form:form>

        <div class="line"></div>
    </div>
</div>

<div id="forgotPasswordModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Wpisz adres e-mail na którym zarejestrowałeś swoje konto a my wyślemy Ci nowe hasło</h4>
            </div>
            <div class="modal-body">
                <spring:url value="/forgotPassword" var="addProjectURL" />
                <form:form action="${addProjectURL}" method="POST" modelAttribute="forgotPasswordForm" cssClass="form-inline">
                    <div class="form-group">
                        <form:input path="email"
                                    placeholder="Adres e-mail" cssClass="form-control"/>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">wyślij</button>
                </form:form>
            </div>
        </div>

    </div>
</div>

<%@include file="../../../footer.jsp" %>