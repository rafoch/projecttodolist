<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@include file="../../../header.jsp" %>

<div class="wrapper">
    <%@include file="../../../sidebar.jsp" %>

    <!-- Page Content Holder -->
    <div id="content">

        <%@include file="../../../navbar.jsp" %>

        <h2 class="text-center header">Witaj ${sessionScope.User.username}!</h2>
        <spring:url value="/users" var="updateURL" />
        <form:form action="${updateURL}" method="POST" modelAttribute="updateForm" cssClass="form-center">
            <form:hidden path="id" />
            <div class="form-group">
                <label>Nazwa użytkownika:</label>
                <form:input path="username" pattern="\w{4,}" required="required"
                            title="Proszę podać minimum 4 znaki bez znaków specjalnych bądź białych"
                            cssClass="form-control"/>
            </div>
            <div class="form-group">
                <label>Adres e-mail:</label>
                <form:input type="email" path="email" cssClass="form-control"/>
            </div>
            <div class="form-group">
                <label>Nowe hasło (opcjonalnie)</label>
                <input type="password" name="newPassword"
                       pattern="\w{8,}" title="Proszę podać minimum 8 znaków"
                       class="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary">Wyślij</button>

        </form:form>

        <div class="line"></div>
    </div>
</div>

<%@include file="../../../footer.jsp" %>