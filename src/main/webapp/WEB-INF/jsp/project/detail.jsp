<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@include file="../../../header.jsp" %>

<div class="wrapper">
    <%@include file="../../../sidebar.jsp" %>

    <!-- Page Content Holder -->
    <div id="content">

        <%@include file="../../../navbar.jsp" %>


        <h2>${project.name}</h2>

        <spring:url value="/tasks" var="addProjectURL"/>
        <form:form action="${addProjectURL}" method="POST" modelAttribute="addTaskForm" cssClass="form-inline">
            ${pageContext.request.contextPath}
            <form:hidden path="id"/>
            <div class="form-group">
                <form:input path="task" pattern="[\w ĄąĆćĘęŁłŃńÓóŚśŹźŻż]{2,}" required="required"
                            title="Zadanie musi być dłuższe niż 1 znak"
                            placeholder="Dodaj zadanie" cssClass="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary">Dodaj</button>
        </form:form>

        <div class="line"></div>

        <div class="table-responsive">
            <table class="table">
                <tbody>
                <c:forEach items="${taskList}" var="task">
                    <tr>
                        <td class="state" style="width: 1%">
                            <span class="task-state" data-id="${task.id}" data-state="${task.done}"><c:choose><c:when
                                    test="${task.done == false}">
                                <span class = "glyphicon glyphicon-remove"></span>
                            </c:when><c:otherwise><span class = "glyphicon glyphicon-ok"></span></c:otherwise></c:choose></span>
                        </td>
                        <c:choose>
                            <c:when test="${task.done == false}">
                                <td class="name">
                             </c:when>
                                 <c:otherwise>
                        <td class="name grey">
                                 </c:otherwise>
                            </c:choose>
                                     <span class="task-name" data-id="${task.id}">${task.task}</span>
                        </td>
                        <td>
                            <div class="pull-right">
                                <a type="button" class="btn btn-default btn-switch">
                                    <span class="glyphicon glyphicon-retweet"></span>
                                </a>
                                <a type="button" class="btn btn-default move up">
                                    <span class="glyphicon glyphicon-chevron-up"></span>
                                </a>
                                <a type="button" class="btn btn-default move down">
                                    <span class="glyphicon glyphicon-chevron-down"></span>
                                </a>
                                <a type="button" class="btn btn-success btn-edit">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                                <a type="button" class="btn btn-danger btn-number" href="/tasks/delete/${task.id}">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>



<script>
    var task_stateSwitch_url = "/api/task/switch";
    function switchState(node,url) {
        var task = $(node).parents('tr').find('.task-state');
        var id = task.data('id');
        var state = task.data('state');
        var data = {id: id, state: state};

        $.post(task_stateSwitch_url, data, function () {
            $(node).parents('tr').find('.name').toggleClass('grey');
            if(state == false){
                var replace_node = "<span  class= 'task-state'  data-id='" + id + "'data-state='" + true + "'><span class = 'glyphicon glyphicon-ok'></span>";
            }
                 else {
                var replace_node = "<span  class= 'task-state'  data-id='" + id + "'data-state='" + false + "'><span class = 'glyphicon glyphicon-remove'></span>";
            }
                $(node).parents('tr').find('.task-state').replaceWith(replace_node);


        }).fail(function () {
            alert(id + " state : " + state);
        })
    }
    (function () {
        $(document).on("click", ".btn-switch", function (event) {
            switchState(this, task_update_url);
        });
    })();
    (function () {
        $(document).on("click", ".state .glyphicon", function (event) {
            switchState(this, task_update_url);
        });
    })();


</script>
<script>
    function confirmTask(node, url) {
        var input = $(node).parents('.input-group').find('input');
        var name = input.val();
        var id = input.data('id');

        var data = {id: id, name: name};
        $.post(url, data, function () {
            var replace_node = "<span data-id='" + id + "' class='task-name'>" + name + "</span>";
            $(node).parents('.input-group').replaceWith(replace_node);
        }).fail(function () {
            alert("Coś się spierdoliło. Bywa xD");
        })
    }

    function rejectTask(node) {
        var input = $(node).parents('.input-group').find('input');
        var id = input.data('id');

        var replace_node = "<span data-id='" + id + "' class='task-name'>" + input.data('old') + "</span>";
        $(node).parents('.input-group').replaceWith(replace_node);
    }
</script>
<script>
    var task_update_url = "/api/task/update";

    (function () {
        $(".btn-edit").on('click', function (event) {
            var task = $(this).parents('tr').find('.task-name');
            var input = "<input type='text' data-old='" + task.text() + "' data-id='" + task.data('id') + "' class='form-control input-task-name' value='" + task.text() + "' />";
            var input_group = '<div class="input-group">\n' + input +
                '      <div class="input-group-btn">\n' +
                '        <button type="button" class="btn btn-default task-confirm"><span class="glyphicon glyphicon-ok"></span></button>\n' +
                '        <button type="button" class="btn btn-default task-reject"><span class="glyphicon glyphicon-remove"></span></button>\n' +
                '      </div>\n' +
                '    </div>';
            task.replaceWith(input_group);
        })
    })();

    (function () {
        $(document).on("keydown", ".input-task-name", function (event) {
            if (event.which == 13) {
                confirmTask(this, task_update_url);
            } else if (event.which == 27) {
                rejectTask(this);
            }
        });
    })();

    (function () {
        $(document).on("click", ".task-confirm", function (event) {
            confirmTask(this, task_update_url);
        });
    })();

    (function () {
        $(document).on("click", ".task-reject", function (event) {
            rejectTask(this);
        });
    })();

    (function () {
        $('table a.move').click(function () {
            var url_task_order_swap = "/api/task/change/order";
            var first_row = $(this).closest('tr');
            var second_row;

            if ($(this).hasClass('up')) {
                second_row = first_row.prev();
                first_row.prev().before(first_row);
            } else {
                second_row = first_row.next();
                first_row.next().after(first_row);
            }

            var first_task_id = first_row.find('.task-name').data('id');
            var second_task_id = second_row.find('.task-name').data('id');

            if (first_task_id === undefined || second_task_id === undefined)
                return;

            var data = {firstTaskId: first_task_id, secondTaskId: second_task_id};

            $.post(url_task_order_swap, data, function () {
            }).fail(function () {
                alert("Coś się spierdoliło. Bywa xD");
            })
        });
    })();
</script>

<%@include file="../../../footer.jsp" %>