<%--
  Created by IntelliJ IDEA.
  User: Carlton Leatch
  Date: 20.10.2017
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<spring:url value="/saveProject" var="addProjectURL" />
<form:form action="${addProjectURL}" method="POST" modelAttribute="addProjectForm">
    <form:hidden path="id"/>
    <table>
        <tr>
            <td>Name:</td>
            <td><form:input path="name"/></td>
        </tr>
        <tr>
            <td></td>
            <td><button type="submit">Save</button> </td>
        </tr>

    </table>
</form:form>
</body>
</html>
