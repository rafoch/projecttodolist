<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@include file="../../../header.jsp" %>

<div class="wrapper">
    <%@include file="../../../sidebar.jsp" %>

    <!-- Page Content Holder -->
    <div id="content">

        <%@include file="../../../navbar.jsp" %>

        <h2>Lista Projektów
            <a type="button" class="btn btn-danger btn-number" data-toggle="modal" data-target="#projectAddModal">
                <span class="glyphicon glyphicon-plus"></span>
            </a>
        </h2>
        <div class="line"></div>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <c:forEach items="${projectList}" var="project">
                    <tr>
                        <td class="name">
                            <spring:url value="projects/${project.id}" var = "projectURL" />
                            <a data-id="${project.id}" class="project-name" href = "${projectURL}">${project.name}</a>
                        </td>
                        <td>
                            <div class="pull-right">
                                <a type="button" class="btn btn-success btn-edit">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                                <spring:url value="projects/delete/${project.id}" var="deleteProjectURL"/>
                                <a type="button" class="btn btn-danger btn-number btn-remove" href="${deleteProjectURL}">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="projectAddModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Dodaj nowy projekt</h4>
            </div>
            <div class="modal-body">
                <spring:url value="/projects" var="addProjectURL" />
                <form:form action="${addProjectURL}" method="POST" modelAttribute="addProjectForm" cssClass="form-inline">
                    <form:hidden path="id"/>
                    <div class="form-group">
                        <form:input path="name" pattern="[\w ĄąĆćĘęŁłŃńÓóŚśŹźŻż]{2,}"
                                    required="required" title="Projekt musi być dłuższy niż 1 znak"
                                    placeholder="Nazwa projektu" cssClass="form-control"/>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Dodaj</button>
                </form:form>
            </div>
        </div>

    </div>
</div>

<script>
    function confirmProject(node, url) {
        var input = $(node).parents('.input-group').find('input');
        var name = input.val();
        var id = input.data('id');

        var data = {id: id, name: name};
        $.post(url, data, function () {
            var replace_node = "<a data-id='" + id + "' class='project-name' href='project/" + id + "'>" + name + "</a>";
            $(node).parents('.input-group').replaceWith(replace_node);
            $('.components li a[data-id=' + id + '] span.name').text(name);
        }).fail(function(){
            alert("Coś się spierdoliło. Bywa xD");
        })
    }

    function rejectProject(node) {
        var input = $(node).parents('.input-group').find('input');
        var id = input.data('id');

        var replace_node = "<a data-id='" + id + "' class='project-name' href='project/" + id + "'>" + input.data('old') + "</a>";
        $(node).parents('.input-group').replaceWith(replace_node);
    }
</script>

<script>
    var project_update_url = "/api/project/update";

    (function() {
        $(".btn-remove").on('click', function(event) {
            var answer = confirm("Czy na pewno chcesz usunąć projekt?");
            if (!answer)
                event.preventDefault();
        })
    })();

    (function() {
        $(".btn-edit").on('click', function(event) {
            var project = $(this).parents('tr').find('.project-name');
            var input = "<input type='text' data-old='" + project.text() + "' data-id='" + project.data('id') + "' class='form-control input-project-name' value='" + project.text() + "' />";
            var input_group = '<div class="input-group">\n' + input +
                '      <div class="input-group-btn">\n' +
                '        <button type="button" class="btn btn-default project-confirm"><span class="glyphicon glyphicon-ok"></span></button>\n' +
                '        <button type="button" class="btn btn-default project-reject"><span class="glyphicon glyphicon-remove"></span></button>\n' +
                '      </div>\n' +
                '    </div>';
            project.replaceWith(input_group);
        })
    })();

    (function() {
        $(document).on("keydown", ".input-project-name", function(event){
            if(event.which == 13) {
                confirmProject(this, project_update_url);
            } else if (event.which == 27) {
                rejectProject(this);
            }
        });
    })();

    (function() {
        $(document).on("click", ".project-confirm", function(event){
            confirmProject(this, project_update_url);
        });
    })();

    (function() {
        $(document).on("click", ".project-reject", function(event){
            rejectProject(this);
        });
    })();
</script>

<%@include file="../../../footer.jsp" %>