<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@include file="../../../header.jsp" %>

<div class="wrapper">
    <%@include file="../../../sidebar.jsp" %>

    <!-- Page Content Holder -->
    <div id="content">

        <%@include file="../../../navbar.jsp" %>

        <h2>Lista Użytkowników
        </h2>
        <div class="line"></div>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <c:forEach items="${userList}" var="user">
                    <spring:url value="/admin/users/${user.id}" var="updateURL" />
                    <tr>
                        <td class="name">
                            <a href="${updateURL}">
                                <span>${user.username}</span>
                            </a>
                        </td>
                        <td>
                            <div class="pull-right">
                                <a href="${updateURL}" type="button" class="btn btn-success btn-edit">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                                <spring:url value="/admin/users/delete/${user.id}" var="deleteURL" />
                                <a href="${deleteURL}" type="button" class="btn btn-danger btn-number btn-remove">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    (function() {
        $(".btn-remove").on('click', function(event) {
            var answer = confirm("Czy na pewno chcesz usunąć użytkownika?");
            if (!answer)
                event.preventDefault();
        })
    })();
</script>

<%@include file="../../../footer.jsp" %>
