package com.todolist.rest;

import com.todolist.model.Task;
import com.todolist.model.User;
import com.todolist.service.ITaskService;
import com.todolist.service.IUserService;
import com.todolist.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = "/api/")
public class TaskRestController {

    @Autowired
    IUserService userService;

    @Autowired
    ITaskService taskService;

    private void isOwnerValidator(HttpSession session, Task task) throws AccessDeniedException {
        if (SessionUtils.getUser(session).getId() != task.getProject().getUser().getId())
            throw new AccessDeniedException("403 returned");
    }

    @RequestMapping(value = "/task/update", method = RequestMethod.POST)
    @ResponseBody
    public void updateProject(HttpSession session, @RequestParam int id,
                              @RequestParam String name) throws AccessDeniedException {
        User user = SessionUtils.getUser(session);

        Task task = taskService.getTaskById(id);

        this.isOwnerValidator(session, task);

        task.setTask(name);
        taskService.saveOrUpdate(task);

    }

    @RequestMapping(value = "/task/switch", method = RequestMethod.POST)
    @ResponseBody
    public void switchTaskState(HttpSession session, @RequestParam int id,
                                @RequestParam boolean state) throws AccessDeniedException{
        User user = SessionUtils.getUser(session);
        Task task = taskService.getTaskById(id);
        this.isOwnerValidator(session,task);
        if(state)
            task.setDone(false);
        else
            task.setDone(true);

        taskService.saveOrUpdate(task);

    }

    @RequestMapping(value = "/task/change/order", method = RequestMethod.POST)
    @ResponseBody
    public void changeTaskOrder(HttpSession session, @RequestParam int firstTaskId,
                              @RequestParam int secondTaskId) throws AccessDeniedException {
        Task firstTask = taskService.getTaskById(firstTaskId);
        Task secondTask = taskService.getTaskById(secondTaskId);


        this.isOwnerValidator(session, firstTask);
        this.isOwnerValidator(session, secondTask);

        int swapValue = firstTask.getOrdering();

        firstTask.setOrdering(secondTask.getOrdering());
        secondTask.setOrdering(swapValue);

        taskService.saveOrUpdate(firstTask);
        taskService.saveOrUpdate(secondTask);
    }

}
