package com.todolist.rest;

import com.todolist.model.Project;
import com.todolist.model.User;
import com.todolist.service.IProjectService;
import com.todolist.service.IUserService;
import com.todolist.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = "/api/")
public class ProjectRestController {

    @Autowired
    IUserService userService;

    @Autowired
    IProjectService projectService;

    @RequestMapping(value = "/project/update", method = RequestMethod.POST)
    @ResponseBody
    public void updateProject(HttpSession session, @RequestParam int id,
                              @RequestParam String name) throws AccessDeniedException {
        User user = SessionUtils.getUser(session);

        Project project = projectService.getProjectById(id);

        // check if editor is project owner
        if (user.getId() != project.getUser().getId()) {
            throw new AccessDeniedException("403 returned");
        }

        project.setName(name);
        projectService.saveOrUpdate(project);

        SessionUtils.updateSidebarProjectList(session, projectService);

    }

}
