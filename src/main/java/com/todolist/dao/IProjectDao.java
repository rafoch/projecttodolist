package com.todolist.dao;

import com.todolist.model.Project;

import java.util.List;

public interface IProjectDao {
    public List<Project> lastProjectsFromUserByTasks(int id);
    public List<Project> allProjectsFromUser(int id);
    public void saveOrUpdate(Project project);
    public Project getProjectById(int id);
    public void delete(int id);
}
