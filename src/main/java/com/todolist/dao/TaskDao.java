package com.todolist.dao;

import com.todolist.model.Task;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskDao implements ITaskDao {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public List<Task> allTasksFromProject(int id) {
        List<Task> list = (List<Task>) getSession().createQuery("FROM Task t where t.project.id= :id order by ordering")
                .setParameter("id",id)
                .list();
        return list;
    }

    private Task getByHighestOrder() {
        return (Task) getSession().createQuery("FROM Task t order by ordering desc ").setMaxResults(1)
                .uniqueResult();
    }

    public void saveOrUpdate(Task task) {
        if (task.getId() == 0) {
            Task orderTask = this.getByHighestOrder();

            if (orderTask == null)
                task.setOrdering(1);
            else
                task.setOrdering(orderTask.getOrdering() + 1);
        }

        getSession().saveOrUpdate(task);
    }

    public Task getTaskById(int id) {
        Task task = (Task) getSession().get(com.todolist.model.Task.class,id);
        return task;
    }

    public void delete(int id) {
       Query q =  getSession().createQuery("DELETE FROM Task t where t.id = :task_Id")
                .setParameter("task_Id", id);
        q.executeUpdate();
    }
    public void deleteWithProject(int id){
        Query q =  getSession().createQuery("DELETE FROM Task t where t.project.id = :project_Id")
                .setParameter("project_Id", id);
        q.executeUpdate();
    }

}
