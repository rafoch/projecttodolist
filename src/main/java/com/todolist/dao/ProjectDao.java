package com.todolist.dao;

import com.todolist.model.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;

import java.util.List;

@Repository
public class ProjectDao implements IProjectDao {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }


    public List<Project> allProjectsFromUser(int id) {
        List<Project> list = (List<Project>) getSession().createQuery("from Project p where p.user.id = :id")
                .setParameter("id", id)
                .list();
        return list;

    }

    public List<Project> lastProjectsFromUserByTasks(int id) {
        List<Project> list = (List<Project>) getSession().createQuery("from Project p where p.user.id = :id order by p.tasks.size desc")
                .setParameter("id", id).setMaxResults(10)
                .list();
        return list;

    }

    public void saveOrUpdate(Project project) {
        getSession().saveOrUpdate(project);
    }

    public Project getProjectById(int id) {
       Project project = (Project) getSession().get(Project.class,id);
        return project;
    }

    public void delete(int id) {
        Query q = getSession().createQuery("DELETE FROM Project p where p.id = :project_Id")
                .setParameter("project_Id", id);
        q.executeUpdate();
    }
}
