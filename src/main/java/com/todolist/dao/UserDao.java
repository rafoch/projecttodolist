package com.todolist.dao;

import com.todolist.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDao implements IUserDao {
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    public List<User> allUsers() {
        List<User> list = (List<User>) getSession().createQuery("FROM User").list();
        return list;
    }

    public void saveOrUpdate(User user) {
        getSession().saveOrUpdate(user);
    }


    public User getUserById(int id) {
        User user = (User) getSession().get(User.class, id);
        return user;
    }

    public void delete(int id) {
        User user = (User)getSession().get(User.class, id);
        getSession().delete(user);
    }

    public List<User> withLogin(String username){
        List<User> user = getSession().createQuery("from User u where u.username = :username")
                .setParameter("username", username)
                .list();
        return user;
    }

    public User getByEmail(String email){
        User user = (User)getSession().createQuery("from User u where u.email = :email")
                .setParameter("email",email)
                .uniqueResult();
        return user;
    }
}
