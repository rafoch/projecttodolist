package com.todolist.dao;

import com.todolist.model.User;

import java.util.List;

public interface IUserDao {

    public List<User> allUsers();
    public void saveOrUpdate(User user);
    public User getUserById(int id);
    public void delete(int id);
    public List<User> withLogin(String username);
    public User getByEmail(String email);
}
