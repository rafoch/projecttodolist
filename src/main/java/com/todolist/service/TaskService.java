package com.todolist.service;

import com.todolist.dao.ITaskDao;
import com.todolist.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {
    private ITaskDao taskDao;

    @Autowired
    public void setTaskDao(ITaskDao taskDao) {
        this.taskDao = taskDao;
    }

    public List<Task> allTasksFromProject(int id) {
        return taskDao.allTasksFromProject(id);
    }

    public void saveOrUpdate(Task task) {
        taskDao.saveOrUpdate(task);
    }

    public Task getTaskById(int id) {
        return taskDao.getTaskById(id);
    }

    public void delete(int id) {
        taskDao.delete(id);
    }

    public void deleteWithProject(int id){taskDao.deleteWithProject(id);}
}
