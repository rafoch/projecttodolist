package com.todolist.service;

import com.todolist.model.User;

import java.util.List;

public interface IUserService {
    public List<User> allUsers();
    public void saveOrUpdate(User user);
    public User getUserById(int id);
    public void delete(int id);
    public List<User> withLogin(String username);
    public boolean isAdmin (User user);
    public User getByEmail(String email);
}
