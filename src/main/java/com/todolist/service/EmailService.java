package com.todolist.service;

import javax.mail.MessagingException;

public interface EmailService {

    public void sendSimpleMessage(String to, String username) throws MessagingException;
    public void sendForgotPasswordMessage(String to, String username, String newPassword) throws MessagingException;
}
