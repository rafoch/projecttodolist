package com.todolist.service;

import com.todolist.model.Task;

import java.util.List;

public interface ITaskService {
    public List<Task> allTasksFromProject(int id);
    public void saveOrUpdate(Task task);
    public Task getTaskById(int id);
    public void delete(int id);
    public void deleteWithProject(int id);
}
