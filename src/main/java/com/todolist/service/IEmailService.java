package com.todolist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.security.SecureRandom;

@Component
public class IEmailService implements EmailService {



    @Autowired
    public JavaMailSender emailSender;

    String forgotSubject = "Przypomnienie hasła do logowania";
    String registerSubject = "Dziękujemy za rejestrację";
    String from = "BytProjectToDoList@gmail.com";


    public void sendSimpleMessage(String to, String username) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        String text = "<h1 style = 'color = #069'>Drogi użytkowniku "+username+".</h1>\n<h3>Bardzo dziękujemy za rejestrację na naszym serwisie.</h3> \n</br> <h3>Pozdrawiamy Team ToDoList</h3>";
        message.setContent(text,"text/html; charset=utf-8");
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(registerSubject);

        emailSender.send(message);
    }

    public void sendForgotPasswordMessage(String to, String username, String newPassword) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        String text = "<h1>Drogi użytkowniku "+username+".</h1>\n<h3>Twoje hasło zostało zmienione na: "+newPassword+".</h3> \n</br> <h3>Pozdrawiamy Team ToDoList</h3>";
        message.setContent(text,"text/html; charset=utf-8");
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(forgotSubject);

        emailSender.send(message);
    }



    public String generatePassword() {
        SecureRandom random = new SecureRandom();
        String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String ALPHA = "abcdefghijklmnopqrstuvwxyz";
        String NUMERIC = "0123456789";
        String dic = ALPHA_CAPS+ALPHA+NUMERIC;
        String result = "";
        for (int i = 0; i < 8; i++) {
            int index = random.nextInt(dic.length());
            result += dic.charAt(index);
        }
        return result;
    }
}
