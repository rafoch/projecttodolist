package com.todolist.service;

import com.todolist.dao.IUserDao;
import com.todolist.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

    private IUserDao userDao;

    @Autowired
    public void setUserDao(IUserDao userDao){
        this.userDao = userDao;
    }

    public List<User> allUsers() {
        return userDao.allUsers();
    }

    public void saveOrUpdate(User user) {
        userDao.saveOrUpdate(user);
    }


    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    public void delete(int id) {
        userDao.delete(id);
    }

    public List<User> withLogin(String username) {
        return userDao.withLogin(username);
    }

    public boolean isAdmin (User user) {
        return user.getRole().equals("admin");
    }

    public User getByEmail(String email) {
        return  userDao.getByEmail(email);
    }
}
