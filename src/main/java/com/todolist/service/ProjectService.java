package com.todolist.service;

import com.todolist.dao.IProjectDao;
import com.todolist.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {
    private IProjectDao projectDao;

    @Autowired
    public void setProjectDao(IProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    public List<Project> lastProjectsFromUserByTasks(int id) {return projectDao.lastProjectsFromUserByTasks(id);};

    public List<Project> allProjectsFromUser(int id) {
        return projectDao.allProjectsFromUser(id);
    }

    public void saveOrUpdate(Project project) {
        projectDao.saveOrUpdate(project);
    }

    public Project getProjectById(int id) {
        return projectDao.getProjectById(id);
    }

    public void delete(int id) {
        projectDao.delete(id);
    }
}
