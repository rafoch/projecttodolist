package com.todolist.utils;

import com.todolist.model.Project;
import com.todolist.model.Task;
import com.todolist.model.User;
import com.todolist.service.IProjectService;

import javax.servlet.http.HttpSession;
import java.util.List;

public class SessionUtils {
    public static User getUser(HttpSession session) {
        return (User) session.getAttribute("User");
    }

    public static Project getProject (HttpSession session) {
        return (Project) session.getAttribute("Project");
    }

    public static void updateSidebarProjectList(HttpSession session, IProjectService projectService) {
        User user = SessionUtils.getUser(session);
        List<Project> projectList = projectService.lastProjectsFromUserByTasks(user.getId());
        session.setAttribute("projectList", projectList);
    }
    public static boolean isLogged(HttpSession session) {
        return session.getAttribute("User") != null;
    }
}
