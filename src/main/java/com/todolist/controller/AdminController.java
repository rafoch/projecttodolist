package com.todolist.controller;

import com.todolist.model.User;
import com.todolist.service.IUserService;
import com.todolist.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    @Autowired
    IUserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private void preValidator(HttpSession session) throws AccessDeniedException {
        if (!userService.isAdmin(SessionUtils.getUser(session)))
            throw new AccessDeniedException("403 returned");
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView userList(HttpSession session) {
        this.preValidator(session);

        ModelAndView model = new ModelAndView("admin/list");

        List<User> userList = userService.allUsers();

        model.addObject("userList", userList);

        return model;
    }

    @RequestMapping(value = "/users/{id}",method = RequestMethod.GET)
    public ModelAndView updateUserForm(HttpSession session, @ModelAttribute("updateForm")
                                            @PathVariable("id") int id) {
        this.preValidator(session);

        ModelAndView model = new ModelAndView("admin/updateForm");
        User user = userService.getUserById(id);

        Map<String,String> roleList = new LinkedHashMap<String,String>();
        roleList.put("user", "Użytkownik");
        roleList.put("admin", "Admin");

        model.addObject("updateForm", user);
        model.addObject("roleList", roleList);

        return model;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ModelAndView updateUser(HttpSession session, @ModelAttribute("updateForm") User user,
                                        @RequestParam String newPassword) throws AccessDeniedException {
        this.preValidator(session);

        if (newPassword.length() > 7)
            user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        else {
            User originUser = userService.getUserById(user.getId());
            user.setPassword(originUser.getPassword());
        }

        userService.saveOrUpdate(user);

        // If admin overwrite himself update session
        if (SessionUtils.getUser(session).getId() == user.getId())
            session.setAttribute("User", user);

        return new ModelAndView("redirect:/admin/users");
    }

    @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteUser(HttpSession session, @PathVariable("id") int id) {
        this.preValidator(session);;

        userService.delete(id);
        return new ModelAndView("redirect:/admin/users");
    }
}
