package com.todolist.controller;

import com.todolist.model.User;
import com.todolist.service.IEmailService;
import com.todolist.service.IProjectService;
import com.todolist.service.IUserService;
import com.todolist.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class UserController {

    @Autowired
    IUserService userService;

    @Autowired
    IProjectService projectService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    IEmailService emailService;

    private void isOwnerValidator(HttpSession session, User user) throws AccessDeniedException {
        if (SessionUtils.getUser(session).getId() != user.getId())
            throw new AccessDeniedException("403 returned");
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registerForm() {
        ModelAndView model = new ModelAndView("admin/registerForm");
        User user = new User();

        model.addObject("registerForm", user);

        return model;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView register(@ModelAttribute("userForm") User user) throws MessagingException {
        if (userService.allUsers().size() == 0)
            user.setRole("admin");
        else
            user.setRole("user");

        if (userService.withLogin(user.getUsername()).size() == 0) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userService.saveOrUpdate(user);
            emailService.sendSimpleMessage(user.getEmail(),user.getUsername());
        } else {
            ModelAndView model = new ModelAndView("admin/registerForm");
            model.addObject("registered", false);
            return new ModelAndView("redirect:/register");
        }

        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView log(@ModelAttribute("loginForm") User user, HttpSession session) {
        List<User> users = userService.withLogin(user.getUsername());

        ArrayList<String> errors = new ArrayList<String>();
        session.setAttribute("errors", errors);

        if (users.size() == 1) {
            User u = users.get(0);
            if (bCryptPasswordEncoder.matches(user.getPassword(), u.getPassword())) {
                session.setAttribute("User", u);
                session.setAttribute("IsAdmin", u.getRole().equals("admin"));

                SessionUtils.updateSidebarProjectList(session, projectService);

                return new ModelAndView("redirect:/projects");
            }
        }
        errors.add("Błędny login lub hasło");
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(HttpSession session) {
        if (!SessionUtils.isLogged(session)) {
            ModelAndView model = new ModelAndView("user/loginForm");
            User user = new User();
            model.addObject("loginForm", user);
            model.addObject("forgotPasswordForm",user);
            return model;
        } else
            return new ModelAndView("redirect:/projects");
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public ModelAndView forgotPassword(@ModelAttribute("forgotPasswordForm")User user) throws MessagingException {
        User u = userService.getByEmail(user.getEmail());
        if(u != null){
            String password = emailService.generatePassword();
            u.setPassword(bCryptPasswordEncoder.encode(password));
            userService.saveOrUpdate(u);
            emailService.sendForgotPasswordMessage(u.getEmail(),u.getUsername(),password);
        }
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpSession session, SessionStatus status) {
        session.invalidate();
        status.setComplete();
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/users/{id}",method = RequestMethod.GET)
    public ModelAndView updateForm(HttpSession session, @ModelAttribute("updateForm") @PathVariable("id") int id) {
        User user = userService.getUserById(id);
        this.isOwnerValidator(session, user);

        ModelAndView model = new ModelAndView("user/updateForm");

        model.addObject("updateForm", user);

        return model;
    }

    @RequestMapping(value = "/users",  method = RequestMethod.POST)
    public ModelAndView update(HttpSession session, @ModelAttribute("updateForm") User user,
                                   @RequestParam String newPassword) throws AccessDeniedException {
        this.isOwnerValidator(session, user);

        User originUser = userService.getUserById(user.getId());

        if (newPassword.length() > 7)
            user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        else
            user.setPassword(originUser.getPassword());

        user.setRole(originUser.getRole());

        userService.saveOrUpdate(user);
        session.setAttribute("User", user);

        return new ModelAndView("redirect:/users/" + user.getId());
    }
}
