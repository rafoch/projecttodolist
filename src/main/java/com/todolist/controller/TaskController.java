package com.todolist.controller;

import com.todolist.model.Project;
import com.todolist.model.Task;
import com.todolist.model.User;
import com.todolist.service.IProjectService;
import com.todolist.service.ITaskService;
import com.todolist.service.IUserService;
import com.todolist.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/tasks")
public class TaskController {

    @Autowired
    IUserService userService;

    @Autowired
    IProjectService projectService;

    @Autowired
    ITaskService taskService;


    private void preValidator(HttpSession session) throws AccessDeniedException {
        Project projectFromSession = SessionUtils.getProject(session);
        User userFromSession = SessionUtils.getUser(session);

        if (projectFromSession.getUser().getId() != userFromSession.getId()) {
            throw new AccessDeniedException("403 returned");
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("id") int id, HttpSession session) {
        this.preValidator(session);

        taskService.delete(id);

        SessionUtils.updateSidebarProjectList(session, projectService);

        Project projectFromSession = SessionUtils.getProject(session);
        return new ModelAndView("redirect:/projects/" + projectFromSession.getId());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("addTaskForm") Task task, HttpSession session) {
        this.preValidator(session);

        Project projectFromSession = SessionUtils.getProject(session);

        task.setProject(projectFromSession);
        taskService.saveOrUpdate(task);

        SessionUtils.updateSidebarProjectList(session, projectService);

        return new ModelAndView("redirect:/projects/" + projectFromSession.getId());
    }


}