package com.todolist.controller;

import com.todolist.model.Project;
import com.todolist.model.Task;
import com.todolist.model.User;
import com.todolist.service.IProjectService;
import com.todolist.service.ITaskService;
import com.todolist.service.IUserService;
import com.todolist.utils.SessionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "/projects")
public class ProjectController {

    @Autowired
    IUserService userService;

    @Autowired
    IProjectService projectService;

    @Autowired
    ITaskService taskService;


    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private void isOwnerValidator(HttpSession session, Project project) throws AccessDeniedException {
        if (SessionUtils.getUser(session).getId() != project.getUser().getId())
            throw new AccessDeniedException("403 returned");
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView list(HttpSession session) {
        if (!SessionUtils.isLogged(session))
            return new ModelAndView("redirect:/");

        User userFromSession = SessionUtils.getUser(session);

        ModelAndView model = new ModelAndView("project/list");
        List<Project> list = projectService.allProjectsFromUser(userFromSession.getId());
        model.addObject("projectList", list);

        Project project = new Project();
        model.addObject("addProjectForm", project);

        SessionUtils.updateSidebarProjectList(session, projectService);

        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("addProjectForm") Project project, HttpSession session) {
        if (!SessionUtils.isLogged(session))
            return new ModelAndView("redirect:/");

        User userFromSession = SessionUtils.getUser(session);
        userFromSession.getProjects().add(project);
        userService.saveOrUpdate(userFromSession);

        project.setUser(userFromSession);
        projectService.saveOrUpdate(project);
        SessionUtils.updateSidebarProjectList(session, projectService);

        return new ModelAndView("redirect:/projects");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView detail(HttpSession session, @PathVariable("id") int id) {
        if (!SessionUtils.isLogged(session))
            return new ModelAndView("redirect:/");

        Project project = projectService.getProjectById(id);

        if (project == null) {
            return new ModelAndView("redirect:/");
        }

        this.isOwnerValidator(session, project);

        ModelAndView model = new ModelAndView("project/detail");
        List<Task> taskList = taskService.allTasksFromProject(id);
        Task task = new Task();
        model.addObject("taskList", taskList);
        model.addObject("project", project);
        model.addObject("addTaskForm", task);
        session.setAttribute("Project", project);

        return model;

    }



    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(HttpSession session, @PathVariable("id") int id) {
        if (!SessionUtils.isLogged(session))
            return new ModelAndView("redirect:/");

        Project project = projectService.getProjectById(id);

        if (project == null) {
            return new ModelAndView("redirect:/projects");
        }

        this.isOwnerValidator(session, project);

        taskService.deleteWithProject(id);
        projectService.delete(id);

        SessionUtils.updateSidebarProjectList(session, projectService);

        return new ModelAndView("redirect:/projects");
    }
}
